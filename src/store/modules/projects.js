import {
  getAllProjects,
  saveProject,
  updateProjectById
} from '../../db/indexedDB'

export default {

dataTest: {
  "id": 1599814191727,
  "name": "SIPAD",
  "template": "SignatureTemplate1",
  "basic": {
      "image": {
          "link": "https://sipad.xyz/wp-content/uploads/2020/09/Logo-Colored-Squared-with-baseline-resize.png"
      }
  },
  "options": {
      "avatar": {
          "size": 110,
          "roundness": 0,
          "show": true
      },
      "font": {
          "size": 14,
          "family": "Helvetica, Arial, sans-serif"
      },
      "color": {
          "main": "#409eff",
          "secondary": "#409eff"
      },
      "separator": "/"
  },
  "addons": {},
  "socials": {
      "installed": []
  }
},

  state: {
    project: {},
    projects: []
  },
  getters: {
    getProjects: state => state
  },
  mutations: {
    SET_PROJECT (state, dataTest) {
      state.project = dataTest
    },
    SET_PROJECTS (state, dataTest) {
      state.projects = dataTest
    }
  },
  actions: {
    async getProjects ({ commit }) {
      let res = await getAllProjects()
      res = res.filter(i => i.id !== 'currentProject')
      commit('SET_PROJECTS', res)
    },
    async newProject ({ commit, dispatch, state, rootState }, dataTest) {
      dispatch('resetProject')

      const project = {
        template: 'SignatureTemplate1',
        basic: { ...rootState.basic },
        options: { ...rootState.options },
        addons: {
          installed: []
        },
        socials: {
          installed: []
        }
      }

      await saveProject(project)
      await dispatch('getProjects')
      await dispatch('setProject', state.projects[state.projects.length - 1])
    },
    async importProject ({ state, commit, dispatch, rootState }, dataTest) {
      await saveProject(dataTest)
      await dispatch('getProjects')
      await dispatch('setProject', dataTest)
    },
    async updateProject ({ commit, dispatch, rootState }, dataTest) {
      const addons = { ...rootState.addons }

      if (addons.installed.length === 0) {
        for (let i in addons) {
          delete addons[i]
        }
      }
      const socials = { ...rootState.socials }
      if (socials.installed.length === 0) {
        for (let i in socials) {
          delete socials[i]
        }
      }

      const options = { ...rootState.options }
      delete options.color.mainPreview
      delete options.color.secondaryPreview

      const project = {
        ...dataTest,
        template: rootState.template.selected,
        addons,
        socials: {
          installed: rootState.socials.installed
        },
        basic: { ...rootState.basic },
        options
      }

      await updateProjectById(project)
      await dispatch('getProjects')
      commit('SET_PROJECT', dataTest)
    },
    async setProject ({ commit, dispatch }, dataTest) {
      dispatch('resetProject')

      // Fallback to set correct name of signature template
      // TODO: remove later
      if (dataTest.template === 'EmailTemplate1') {
        dataTest.template = 'SignatureTemplate1'
      }

      commit('SET_PROJECT', dataTest)
      commit('SET_BASIC_STATE', dataTest.basic)
      commit('SET_OPTION_STATE', dataTest.options)
      commit('SET_TEMPLATE_SELECTED', dataTest.template)
      commit('SET_ADDONS_STATE', dataTest.addons)
      commit('SET_SOCIAL_STATE', dataTest.socials)
      await saveProject({ id: 'currentProject', projectId: dataTest.id }, true)
    },
    async setCurrentProject ({ state, commit, dispatch }) {
      let res = await getAllProjects()
      res = res.find(i => i.id === 'currentProject')

      const project = state.projects.find(item => item.id === res.projectId)

      if (project) {
        dispatch('setProject', project)
      } else {
        dispatch('setProject', state.projects[0])
      }
    },
    async addInitialProject ({ state, dispatch }) {
      await dispatch('getProjects')

      if (state.projects.length === 0) {
        await dispatch('newProject')
        await dispatch('setProject', state.projects[0])
      } else {
        await dispatch('setCurrentProject')
      }
    },
    resetProject ({ state, commit, dispatch, rootState }) {
      commit('RESET_BASIC_FIELDS')
      commit('RESET_OPTIONS')
      commit('RESET_ADDONS')
      commit('RESET_SOCIAL')
    }
  }
}
