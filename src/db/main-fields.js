import { guid } from '../util/helpers'

const fields = {
  basic: []
}

function seed () {
  const basic = [
    { name: 'Prénom NOM', type: 'text', value: 'Vincent SEGARD' },
    { name: 'Fonction', type: 'text', value: 'Directeur Technique' },
    { name: 'Société', type: 'text', value: 'SIPAD' },
    { name: 'Email : ', type: 'email', value: 'v.segard@sipadconnect.fr' },
    { name: 'Tél : ', type: 'tel', value: '01 44 88 22 38' },
    { name: 'Site : ', type: 'link', value: 'sipad.xyz' },
    { name: '', type: 'text', value: '48 rue Louis Rolland 92120 Montrouge'}
  ]

  basic.forEach(item => {
    fields.basic.push({ ...item, id: guid() })
  })

  addPlaceholder()
}

function addPlaceholder () {
  const placeholders = [
    'Vincent SEGARD',
    'Directeur Technique',
    'SIPAD',
    'v.segard@sipadconnect.fr',
    '01 44 88 22 38',
    'sipad.xyz',
    '48 rue Louis Rolland 92120 Montrouge'
  ]

  fields.basicPlaceholder = [
    ...fields.basic.map((item, index) => {
      return { ...item, value: placeholders[index] }
    })
  ]
}

seed()

export default fields
